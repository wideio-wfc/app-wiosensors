##
## A Physical sensor provide data about the realworld
## most of the time the sensor does not

class SensorLocation:
    latitude = FloatField()
    longitude = FloatField()


@dynamicmodel(SensorLocation)  # ML model for storing location model
@wideio_model
class Sensor:
    # data streams
    network_address = StringField() # ipv4/ipv6/...
    interval = FloatField(help_text="interval of data")
    update_mode = StringField(choices=['poll', 'hook'])
    data_type = StringField(choices=['enumerated', 'scalar', 'vectorial', 'matricial', 'tensor'])

    # onboarded software (considered as an invididual tnode)
    # attached_tnode = ForeignKey('TNode', null=True)
